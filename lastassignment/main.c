/*
 *  Aaron Brainard
 *  CS4540 Kernel Module
 *  Friday, December 9, 2016
 *
 *  I had a lot of help from here:
 *
 *    http://books.gigatux.nl/mirror/networksecuritytools/0596007949/networkst-CHP-7-SECT-2.html
 *
 *  As far as intercepting system calls goes.
 *
 ***********************************************************/








/* Declare what kind of code we want from the header files */
#define __KERNEL__         /* We're part of the kernel */
#define MODULE             /* Not a permanent part, though. */
#define _LOOSE_KERNEL_NAMES
    /* With some combinations of Linux and gcc, tty.h will not compile if
       you don't define _LOOSE_KERNEL_NAMES.  It's a bug somewhere.
    */


/* Standard headers for LKMs */
#include </usr/src/linux-headers-4.4.0-51-generic/include/config/modversions.h> 
#include </usr/src/linux-headers-4.4.0-51/include/linux/module.h>  
#include </usr/src/linux-headers-4.4.0-51/include/linux/kernel.h>
#include </usr/src/linux-headers-4.4.0-51/include/linux/tty.h>      /* console_print() interface */
#include </usr/src/linux-headers-4.4.0-51/include/linux/user.h>
#include </usr/src/linux-headers-4.4.0-51/include/linux/init.h>
#include </usr/src/linux-headers-4.4.0-51/include/linux/syscalls.h>
#include </usr/src/linux-headers-4.4.0-51/include/uapi/linux/unistd.h>
#include </usr/src/linux-headers-4.4.0-51/include/linux/proc_fs.h>
#include </usr/src/linux-headers-4.4.0-51/include/linux/uaccess.h>
#include <sys/utsname.h>
#include </usr/src/linux-headers-4.4.0-51/include/linux/namei.h>



unsigned long *sys_call_table;


/* Initialize the LKM */
int init_module()
{
  int i = 1024; //max number of attempts
  int flag = 0;
  unsigned long *sys_table;
  sys_table = (unsigned long*)&system_utsname;

  while(i && flag) {
    if(sys_table[__NR_read] == (unsigned long)sys_read) {
      sys_call_table = sys_table;
      flag = 1;
    }
    i--;
    sys_table++;
  }

  if(flag) {
    //do stuff
  }










  /* If we return a non zero value, it means that 
   * init_module failed and the LKM can't be loaded 
   */
  return 0;
}


/* Cleanup - undo whatever init_module did */
void cleanup_module()
{
  printk("Short is the life of an LKM\n");
}

void she_to_he(char * str)
{
  if(!strncmp(str,"she",3)) {
    //str[0] = " ";
  }
}