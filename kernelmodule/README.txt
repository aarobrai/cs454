CS 4540 Kernel Filter Assignment
Aaron Brainard
Monday, December 12, 2016

Intro:

	For the "gender bender" I used a loadable module which was inserted into the kernel of Ubuntu 11.04 on Virtualbox. The module "interceptor.ko" intercepts the read() system call and changes instances of "she" to "he" by replacing the 's' with a ' ' character.

Insertion into kernel:

	The Makefile automatically finds the kernel source tree on the virtual machine. When "make" is run, it enters the /usr/src/linux-headers.... directory to find what it needs. Then the module is built. This produces a number of files including interceptor.ko, which is the actual module file inserted into the kernel.

	The module is loaded using the command 
		
		$lsmod interceptor.ko 

	and removed using 

		$rmmod interceptor.ko

Design:

	The basic design for the module was obtained from:

		http://r00tkit.me/?p=46

	This page is an article by Ciaran McNally, titled "Hijacking System Calls With Loadable Kernel Modules".

	The full original code can be found here:

		https://github.com/maK-/Syscall-table-hijack-LKM

	The code provides a template for hijacking the "write" system call and forcing the kernel to run our own implementation whenever write() is called. As such, I had to make modifications in order to use the read() system call instead.

	In the source directory, there is a bash script in the "scripts" folder. The script finds the address of the system call table and inserts it into the interceptor.c file, which is used to build the module. Modifications to the source are entered in template.c, and when "make" is run the script injects the addresses into interceptor.c and the rest of the code.

	When the module is initialized, we must execute the line of code:

		write_cr0 (read_cr0 () & (~ 0x10000));

	Which turns off a write protection bit, allowing us to make changes to the system call table. We use this to modify the system call table at [__NR_read] and place our "hijacked" read() into the system call table. The system call table at [__NR_read] now points to our new_read() function. After this we execute the line:

		write_cr0 (read_cr0 () | 0x10000);

	Which re-enables write protection on the system call table. We use these same two commands when exiting the module, placing the original system read() call back into the system call table.

	Again, credit goes to the original author for the template and makefile, as cited above. 

	From here, changing the behavior of the read() call is fairly simple. The prototype for new_read() is:

		asmlinkage size_t new_read(unsigned int, void *, ssize_t)

	which is basically the same as the read system call. It takes in a file descriptor, a buffer, and a count. When new_read() is called, we verify that we have read access using access_ok() and passing VERIFY_READ as a parameter. This information was obtained from

		http://www.ibm.com/developerworks/library/l-kernel-memory-access/

	Which is an article titled "User space memory access from the Linux kernel" written by M. Jones. Then we make sure the buffer isn't blank, which I did to make debugging easier. The buffer is then copied into newBuf, a non-const kernel memory char* variable to which we can make changes. Using strstr we find all occurences of " she " in the buffer and replace the 's' with a blank ' ', resulting in '  he '. Then memcpy() is used to copy the modified newBuf back into the original buffer, which is itself copied back to user space.

In order to make output somewhat easier to interpret, I included <linux/sched.h> in order to print the process name that is currently using the new_read() call. I obtained that info from user raghav3276 on this stackoverflow post: 

	http://stackoverflow.com/questions/26451729/how-to-get-process-id-name-and-status-using-module

