#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

int main(int argc, char *argv[])
{
	int fd;
	int n;
	char str[80];

	fd = open("testfile.txt",O_RDONLY);
	while(n = read(fd,str,80)) {
		str[n] = '\0';
		printf("%s\n",str);
	}

	close(fd);
	return 0;
}
