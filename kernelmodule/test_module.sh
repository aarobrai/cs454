#!/bin/bash
# load interceptor module, run client program which reads test txt file, and remove module

echo "> Original file:"
#cat testfile.txt
grep "" testfile.txt
#./client
echo "> Loading module..."
echo "> Modified file:"
sudo insmod interceptor.ko
#cat testfile.txt
grep "" testfile.txt
#./client
echo "> Unloading module..."
sudo rmmod interceptor.ko
echo "> finished"
