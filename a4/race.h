void* thread_handler(void *ptr);

void getSemsInFirst( sem_t *stdin_sem, sem_t *stdout_sem, int id);

void getSemsOutFirst( sem_t *stdin_sem, sem_t *stdout_sem, int id);