/*
*
* CS 4430 A4
* Author: Aaron Brainard
* Date: 10/31/16
* 
* The threads don't always necessarily ask for input
* in the order 1,2,3,4. I imagine there's some other
* magic at work there
*****************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>  
#include <fcntl.h>   
#include <pthread.h>    
#include <string.h>     
#include <semaphore.h>  
#include <sys/stat.h>  
#include <time.h>
#include <unistd.h>
#include "race.h"

#define TIMEOUT 3; 

sem_t stdin_sem,stdout_sem;

int main(int argc, char* argv[])
{

	pthread_t thread_a,thread_b,thread_c,thread_d;
	int id[4] = {1,2,3,4};
	int i;

	sem_init(&stdin_sem,0,1);
	sem_init(&stdout_sem,0,3);	


	pthread_create(&thread_a, NULL, &thread_handler, &id[0]);
	pthread_create(&thread_b, NULL, &thread_handler, &id[1]);
	pthread_create(&thread_c, NULL, &thread_handler, &id[2]);
	pthread_create(&thread_d, NULL, &thread_handler, &id[3]);


	pthread_join(thread_a,NULL);
	pthread_join(thread_b,NULL);
	pthread_join(thread_c,NULL);
	pthread_join(thread_d,NULL);

	//cleanup
	sem_destroy(&stdin_sem);
	sem_destroy(&stdout_sem);
}

void* thread_handler(void *ptr)
{

	int id;
	char line[256]; //for user input

	//get thread id
	id = *((int *) ptr);

	do {
		if(id == 1 || id == 2){
			getSemsInFirst(&stdin_sem,&stdout_sem,id);			
		}else if (id == 3 || id == 4){
			getSemsOutFirst(&stdin_sem,&stdout_sem,id);
		}else{
			printf("Unexpected thread id\n");
		}

		printf("Thread %d: Enter a message: \n",id);
		fgets(line,sizeof(line),stdin);
		printf("Thread %d input: %s \n",id,line);

		//release both semaphores
		//printf("Thread %d releasing semaphores...\n",id);
		sem_post(&stdin_sem);
		sem_post(&stdout_sem);


	}while(strcmp(line,"q\n"));

	printf("Thread %d: Bye\n",id);
	pthread_exit(NULL);

}

/*
 *	Borrowed this from Hardin's email, with a few modifications
 *  This function asks for stdin_sem first, will wait for the
 *  stdout_sem and timeout if it doesn't get it.
 */
void getSemsInFirst( sem_t *stdin_sem, sem_t *stdout_sem, int id) {

	int not_ok = 1;
	int res;
	//set up sem-wait params
	//I learned about this from the sem_timedwait man page
	struct timespec ts;
  	clock_gettime(CLOCK_REALTIME, &ts);
	ts.tv_sec += TIMEOUT;

    while(not_ok) {

        // ask for stdin_sem  wait for it
    	sem_wait(stdin_sem);                
        //  ask for stdout_sem wait for it with timer
		res = sem_timedwait(stdout_sem,&ts);
		if(res == -1){
			//couldn't get stdout_sem, release stdin_sem then wait random
			sem_post(stdin_sem);
            sleep((rand() % 10) + 1);
		}else{
			not_ok = 0;
		}
    } // end while

}


/*
 *	Borrowed this from Hardin's email, with a few modifications
 *  This is the stdout first version. 
 *  
 */
void getSemsOutFirst( sem_t *stdin_sem, sem_t *stdout_sem, int id) {

	int not_ok = 1;
	int res;
	//set up sem-wait params
	struct timespec ts;
  	clock_gettime(CLOCK_REALTIME, &ts);
	ts.tv_sec += TIMEOUT;


    while(not_ok) {

        // ask for stdout_sem  wait for it
    	sem_wait(stdout_sem);

                
        //  ask for stdin_sem wait for it with timer
		res = sem_timedwait(stdin_sem,&ts);
		if(res == -1){
			//couldn't get stdin_sem, release stdout_sem then wait random
			sem_post(stdout_sem);
            sleep((rand() % 10) + 1);
		}else{
			not_ok = 0;
		}

    } // end while

}




