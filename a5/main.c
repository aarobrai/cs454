/*
*
* CS 4430 A5
* Author: Aaron Brainard
* Date: 11/08/16
* 
* 
* 
* 
*****************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>  
#include <fcntl.h>   
#include <pthread.h>    
#include <string.h>     
#include <semaphore.h>  
#include <sys/stat.h>  
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#include "race.h"

#define TIMEOUT 3

int main(int argc, char* argv[])
{
	sem_t stdin_sem,stdout_sem;
	pid_t pid[4];
	int i;
	int result;
	int status;
	int not_done = 1;
	char* line;

	if(sem_init(&stdin_sem,0,1) == -1){
		perror("stdin_sem init error");
	}
	if(sem_init(&stdout_sem,0,2) == -1){
		perror("stdout_sem init error");
	}	



	//loop in parent and fork
	for(i = 0; i < 4; i++) {

		pid[i] = fork();

		if(pid[i] < 0) {
			perror("Fork error");
		}else if(pid[i] == 0) {
			//child process
			do {

				if(pid[i] % 2 == 0) {
					get_sems(&stdin_sem,&stdout_sem);
				}else{
					get_sems(&stdout_sem,&stdin_sem);
				}

				printf("Child process %d: Enter a message or press q to quit: \n",i);
				fgets(line,sizeof(line),stdin);
				printf("You entered: %s \n",line);

				//release both semaphores

				
				if((result = sem_post(&stdin_sem)) < 0){
					perror("Error giving back stdin_sem");
				}
				if((result = sem_post(&stdout_sem)) < 0){
					perror("Error giving back stdout_sem");
				}	
						

			}while(strcmp(line,"q\n"));
			printf("Child process %d: Bye\n",i);
			exit(0);


		}else{
			//parent side
			while(wait(&status) > 0);
		}
	}




	//cleanup
	if(sem_destroy(&stdin_sem) == -1){
		perror("sem_destroy error stdin");
	}
	if(sem_destroy(&stdout_sem) == -1){
		perror("sem_destroy error stdout");
	}
}

/*
 *
 *  
 *  
 */


void get_sems( sem_t *first, sem_t *second) 
{

	int not_ok = 1;
	int res;
	//set up sem-wait params
	//I learned about this from the sem_timedwait man page
	struct timespec ts;
  	clock_gettime(CLOCK_REALTIME, &ts);
	ts.tv_sec += TIMEOUT;

    while(not_ok) {

        // ask for first sem wait for it
    	sem_wait(first);                
        //  ask for second sem wait for it with timer
		res = sem_timedwait(second,&ts);
		if(res == -1){
			//couldn't get second sem, release first then wait random
			if(sem_post(first) < 0) {
				perror("sem_post error");
			}
            sleep((rand() % 10) + 1);
		}else{
			not_ok = 0;
		}
    } // end while
    
}





