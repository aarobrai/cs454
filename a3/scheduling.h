#define N 48
#define MAX_CPU_TIME 5 /*try different times*/

struct Process{

	int pid; /* name */
	char type[20]; /*CPU bound, IO bound, or equal*/
	int req_cpu_time; /*before/between IO */
	int req_io_time;  /*time between IO and CPU */
	int time_left; /* How many times the loop runs before this process exits */
	int cur_time_cpu;
	int cur_time_io;
	int cur_time_ready;
	int tot_time_cpu;
	int tot_time_io;
	int tot_time_ready;
	int min_time_ready;
	int max_time_ready;
	int original_priority;
	int current_priority;

};

int scheduler_start(struct Process ps[N]);
int sort(struct Process ps[N]);
int p_ready(struct Process *p);
int p_wait(struct Process *p);
int p_cpu(struct Process *p);
int all_processes_completed(struct Process ps[N]);
void print_queue();
void print_stats(struct Process ps[N]);




















