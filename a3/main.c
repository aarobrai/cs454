/*
*
* CS 4430 A3
* Author: Aaron Brainard
* Date: 10/13/16
* 
*******************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "scheduling.h"

#define BIG_NUM 0xFF; /* To initialize min counters, so they find min properly */

int main(int argc, char* argv[])
{
	int i,j;

	int count = 0;
	struct Process ps[N];	

	/*define 3*16 processes*/
	for(i = 0; i < 16; i++){



		for(j = 0; j < 3; j++){


			ps[count].pid = count;
			ps[count].time_left = 5;
			ps[count].original_priority = i;
			ps[count].current_priority = ps[count].original_priority;
			ps[count].cur_time_cpu = 0;
			ps[count].cur_time_io = 0;
			ps[count].cur_time_ready = 0;
			ps[count].tot_time_cpu = 0;
			ps[count].tot_time_io = 0;
			ps[count].tot_time_ready = 0;
			ps[count].min_time_ready = BIG_NUM;
			ps[count].max_time_ready = 0;

		
			if(j == 0){
			/* high CPU */
				strcpy(ps[count].type,"CPU");
				ps[count].req_cpu_time = 60;
				ps[count].req_io_time = 10;

			}else if (j == 1){
			/* 50/50 */
				strcpy(ps[count].type,"Equal");
				ps[count].req_cpu_time = 40;
				ps[count].req_io_time = 40;

			}else if (j == 2){
			/* High IO */
				strcpy(ps[count].type,"I/O");
				ps[count].req_cpu_time = 10;
				ps[count].req_io_time = 60;

			}
			
			count++;
		}

	}

	scheduler_start(ps);
	return 0;
}





