/*
*
* CS 4430 A3
* Author: Aaron Brainard
* Date: 10/13/16
*
*
* Description: This source file houses the functions for the scheduler.
* Currently we are using the scheduler algorithm discussed in class; only
* one process is in CPU at a time, and there is zero kernel overhead (unrealistic).
* At each iteration, processes in the ready queue without the highest priority value
* (15) have their priority values incremented to ensure that all process are run eventually.
* When a process finishes IO, it is put back into the ready queue with its initial priority.
* When a process reaches max allowable CPU time, it is also kicked back into the ready queue
* with its initial priority.
* 
* The way the structs and times are defined now, the algorithm seems to favor CPU bound processes...
* 
*******************************/
#include <stdio.h>
#include <stdlib.h>
#include "scheduling.h"

#define P_FINISHED -0xFF

int scheduler_start(struct Process ps[N])
{
	int i;

	while(!all_processes_completed(ps)){
		sort(ps);
		for(i = 0; i < N; i++){
			if(i == 0){
				p_cpu(&ps[i]);
			}
			if(ps[i].current_priority >= 0){
				p_ready(&ps[i]);
			}else if(ps[i].current_priority == -1){
				p_wait(&ps[i]);
			}
		}

		//print_queue(ps);

	}

	printf("===== ALL PROCESSES HAVE FINISHED =====\n");
	print_stats(ps);
	return 0;

}



/*
 * Priority queue sort. Processes with the same priority will end up in ascending order within
 * their own priority, but overall will be sorted in descending order by priority. If that makes 
 * any sense
 *
 * Inspiration for this sort was gained from
 * 		http://www.dailyfreecode.com/code/implements-priority-queue-array-2840.aspx
 * specifically in the 'add' method starting around line 75
 */
int sort(struct Process ps[N])
{
	int i,j;
	for(i = 0; i < N; i++){
		for(j = i+1; j < N; j++){
			if(ps[i].current_priority < ps[j].current_priority){
				struct Process temp = ps[j];
				ps[j] = ps[i];
				ps[i] = temp;
			}else if(ps[i].current_priority >= ps[j].current_priority){
				//DO NOT SWAP IDENTICAL KEYS
			}			
		}

	}
	return 0;

}

/*
 * Update IO counters. If IO is finished, send back to 
 * ready queue. If all iterations completed, mark finished
 */
int p_wait(struct Process *p)
{
	p->cur_time_io++;
	p->tot_time_io++;
	if(p->cur_time_io == p->req_io_time){
		p->time_left--;
		p->cur_time_cpu = 0;
		p->cur_time_io = 0;
		p->cur_time_ready = 0;
		//back into ready queue
		p->current_priority = p->original_priority;

		if(p->time_left == 0){
			p->current_priority = P_FINISHED;
			printf("Process %2d has finished \n",p->pid);
		}
	}
	return 0;
}

/*
 * Update timers for time spent in ready queue, and increment
 * lower priority values
 */
int p_ready(struct Process *p)
{
	p->cur_time_ready++;
	p->tot_time_ready++;
	if(p->cur_time_ready > p->max_time_ready){
		p->max_time_ready = p->cur_time_ready;
	}
	if(p->cur_time_ready < p->min_time_ready){
		p->min_time_ready = p->cur_time_ready;
	}
	if(p->current_priority < 15){
		p->current_priority++;
	}
	return 0;
}

/*
 * Increment time in cpu and send the process either back to ready or to IO
 */
int p_cpu(struct Process *p)
{
	p->cur_time_cpu++;
	p->tot_time_cpu++;
	if(p->cur_time_cpu == MAX_CPU_TIME){
		//p->cur_time_cpu = 0;
		p->cur_time_io = 0;
		p->cur_time_ready = 0;
		//back into ready queue
		p->current_priority = p->original_priority;
	}
	if(p->cur_time_cpu >= p->req_io_time){
		p->current_priority = -1;
	}
	return 0;

}

/*
 * Returns 0 if any of the processes are not finished
 */
int all_processes_completed(struct Process ps[N])
{
	int retval = 1;
	int i;
	for(i = 0; i < N; i++){
		if(ps[i].current_priority != P_FINISHED){
			retval = 0;
		}
	}
	return retval;
}

/*
 * Print the queue at the current time
 */
void print_queue(struct Process ps[N])
{
	int i;
	printf("PID CPRI OPRI\n");
	for(i = 0; i < N; i++){
		if(ps[i].current_priority != P_FINISHED){
			printf("%3d %3d %3d \n",ps[i].pid,ps[i].current_priority,ps[i].original_priority);			
		}

	}
}

/*
 * Gives the report after all processes have finished.
 * Maybe change this so it prints a single process' info 
 * as it finishes
 */
void print_stats(struct Process ps[N])
{
	int i;
	for(i = 0; i < N; i++){
		printf("****************************************\n");
		printf("* PID %d \n",ps[i].pid);
		printf("* BOUND %s \n",ps[i].type);
		printf("* TOTAL CPU TIME %d \n",ps[i].tot_time_cpu);
		printf("* TOTAL I/O TIME %d \n",ps[i].tot_time_io);
		printf("* TOTAL READY TIME %d \n",ps[i].tot_time_ready);
		printf("* MAX READY TIME %d \n",ps[i].max_time_ready);
		printf("* MIN READY TIME %d \n",ps[i].min_time_ready);
		//printf("****************************************\n");			
	}

}


